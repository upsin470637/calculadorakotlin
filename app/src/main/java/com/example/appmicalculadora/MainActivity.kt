package com.example.appmicalculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    private lateinit var txtUsuario: EditText
    private lateinit var txtPassword: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        eventoClick()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtPassword = findViewById(R.id.txtPassword)
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun eventoClick() {
        btnIngresar.setOnClickListener {
            val usuario: String = getString(R.string.user).toLowerCase() // Convertir a minúsculas
            val pass: String = getString(R.string.pass)

            val inputUser = txtUsuario.text.toString().trim().toLowerCase() // Convertir a minúsculas
            val inputPass = txtPassword.text.toString().trim()

            if (inputUser == usuario && inputPass == pass) {
                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("usuario", getString(R.string.nombre)) // Puedes ajustar según necesites

                txtUsuario.setText("")
                txtPassword.setText("")
                startActivity(intent)
            } else {
                Toast.makeText(this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show()
            }
        }

        btnSalir.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(this@MainActivity)

            alertBuilder.setMessage("¿Desea salir de la aplicación?")
            alertBuilder.setTitle("Salir")
            alertBuilder.setPositiveButton("Si") { _, _ ->
                finish()
            }

            alertBuilder.setNeutralButton("No") { _, _ -> }

            val box = alertBuilder.create()
            box.show()
        }
    }
}
